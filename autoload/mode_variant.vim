" mode_variant.vim: because this isn't emacs
" vim:fdm=marker:ts=2:sts=2:sw=2:et:tw=100
"
" Plugins like vim-move add nifty functionality, but don't always have obvious keymappings.  Do we
" really need yet another sequence/modifier+hjkl command mapped?  This isn't Emacs.
"
" Instead, we'd like Vim-style modes for groups of related commands.  This turns out to be
" difficult to implement, so mode variants are a better compromise.  mode_variant.vim implements
" these by injecting itself into mappings to emulate the existence of new modes within the existing
" modes.
"
" A dictionary describing the modes that mode_variant.vim should handle should be passed to
" mode_variant#add_modes.  Note that mode_variant.vim needs to know when to reset the mode to its
" 'default' variant, so this dictionary must include all the keys that enter a mode.
"
" The dictionary contains keys for each true mode (e.g. 'n' for normal).  The value of each key is
" a dictionary containing the variants of that mode.  Each variant is a dictionary containing
" actions to execute for each key:
"  - 'mode' & 'variant' specify that future inputs in 'mode' should be treated as part of
"    'variant'.  'mode' defaults to the current mode, and 'variant' defaults to
"    'default'.  If neither is given, no change occurs.
"  - 'map' & 'noremap' execute mappings as though configured through :map or :noremap,
"    respectively.
"  - 'expr' evaluates the expression.  An empty 'map' or 'noremap' key controls how the result is
"    interpreted.
"
" Be consistent with modes.  mode_variant.vim doesn't know that 'v' modes are intended to work in
" 'x'.  It tracks the state of each mode using the 'mode' keys, and only checks mappings using the
" mode given in the mapping.
"
" The 'default' variant has special meaning.  It will be selected by default when a mapping enters
" a mode without specifying which variant should be used; it will also be used if a mapping cannot
" be found in the current variant.
"
" Basic statusline integration is supported.  mode_variant#fuzzy_variant() looks up the active
" variant of the specified mode and returns its name.  If no variants are defined for the current
" mode, it returns 'default'.  mode_variant#on_variant_change() can be used to request a callback
" when the modes changes; the callback will be passed the affected mode and the new variant.
"
" Example: a vim-move variant for visual mode:
" call mode_variant#add_modes({
" \  'n': {
" \   'default': {
" \    'v': { 'mode': 'v', 'noremap': 'v' },
" \    'V': { 'mode': 'v', 'noremap': 'V' },
" \    '<C-v>': { 'mode': 'v', 'noremap': '<C-v>' }
" \   }
" \  },
" \  'v': {
" \   'default': {
" \    '<Leader>m': { 'variant': 'move' }
" \    'h': { 'noremap': 'h' },
" \    'j': { 'noremap': 'j' },
" \    'k': { 'noremap': 'k' },
" \    'l': { 'noremap': 'l' }
" \   },
" \   'move': {
" \    '<CR>': { 'variant': 'default' },
" \    'h': { 'map': '<Plug>MoveBlockLeft' },
" \    'j': { 'map': '<Plug>MoveBlockDown' },
" \    'k': { 'map': '<Plug>MoveBlockUp' },
" \    'l': { 'map': '<Plug>MoveBlockRight' }
" \   }
" \  }
" \ })
"
" mode_variant.vim can plug into keymap_dsl.vim.  It adds several new commands corresponding to
" fields in the mode dictionaries that mode_variant#add_modes() uses:
"
" :MVVariant:
"   Limits the enclosed mappings to the specified variant.
" :MVSetMode:
"   Indicates that the enclosed mappings change to the specified mode. ('mode' key)
" :MVSetVariant:
"   Indicates that the enclosed mappings change to the specified variant. ('variant' key)
"
" :MVMap:
"   Creates mappings to the specified key sequences. ('map' key)
" :MVNoremap:
"   Creates noremap mappings to the specified key sequences. ('noremap' key)
" :MVCall:
"   Creates mappings that call the specified function. ('call' key)
"
" Example: a vim-move variant for visual mode:
" call keymap_dsl#load()
" call mode_variant#load_dsl()
" Key! v V <C-v>|Mode! n|MVSetMode! v|MVNoremap v V <C-v>
" Mode v
"   Key h j k l
"     MVVariant! default|MVNoremap h j k l
"     MVVariant! move|MVMap
"       \ <Plug>MoveBlockLeft <Plug>MoveBlockDown
"       \ <Plug>MoveBlockUp <Plug>MoveBlockRight
"   Pop
"   Key! <Leader>m|MVSetMode! move|MVNoremap <Nop>
"   Key! <CR>|MVVariant! move|MVSetVariant! default|MVNoremap <Nop>
" Pop
" call keymap_dsl#apply_global()
" call mode_variant#unload_dsl()
" call keymap_dsl#unload()

" FIXME: instead of routing everything through a function, create and destroy mappings when variant
" changes.
" TODO: document vs kana/vim-submode
" TODO: 'resets_variant'/'leaves_mode'?
" TODO: variant display (see kana/vim-submode)
" TODO: reset on unknown key? (implies default vs fallback variants)

if exists('g:mode_variant_loaded')
  finish
endif
let g:mode_variant_loaded = v:true

let s:modes = {}
let s:active_variant = {}
let s:variant_notifiers = []

function! s:init_buf()
  " The dictionary referenced by s:modes is shared by b:mode_variants in all buffers without
  " buffer-local mappings.  Buffer-local mappings always use a copy of the dictionary and do not
  " affect the dictionary referenced by s:modes.
  if !exists("b:mode_variants")
    let b:mode_variants = s:modes
  endif
endfunction
autocmd BufWinEnter * call s:init_buf()

function! s:queue_noremap(map)
  let s:current_noremap = a:map
  return "\<Plug>MVNoremap"
endfunction

function! s:handle_noremap()
  if !exists('s:current_noremap')
    return
  endif

  let l:ret = s:current_noremap
  unlet s:current_noremap
  return l:ret
endfunction
for s:mode in map(range(9), {i->'nvxsoilct'[i]})
  noremap <expr> <Plug>MVNoremap <SID>handle_noremap()
endfor
unlet s:mode

function! s:handle_input(mode, input)
  if !has_key(b:mode_variants, a:mode)
    return s:queue_noremap(a:input)
  endif

  let l:mode = b:mode_variants[a:mode]
  let l:varname = has_key(s:active_variant, a:mode) ? s:active_variant[a:mode] : 'default'
  if !has_key(l:mode, l:varname)
    echoerr "No variant" l:varname "in mode" a:mode
    return s:queue_noremap(a:input)
  endif

  " XXX should this be 'default' or e.g. 'any'?
  let l:variant = l:mode.default
  if has_key(l:mode, l:varname) && has_key(l:mode[l:varname], a:input)
    let l:variant = l:mode[l:varname]
  endif
  if !has_key(l:variant, a:input)
    echoerr "No key" a:input "in mode" a:mode.'/'.l:varname
    return s:queue_noremap(a:input)
  endif

  let l:map = l:variant[a:input]
  if has_key(l:map, 'mode') || has_key(l:map, 'variant')
    let l:dest_mode = has_key(l:map, 'mode') ? l:map.mode : a:mode
    let l:dest_var = has_key(l:map, 'variant') ? l:map.variant : 'default'
    let l:changed = l:dest_var != get(s:active_variant, l:dest_mode, 'default')
    let s:active_variant[l:dest_mode] = l:dest_var

    if l:changed
      for l:Notify in s:variant_notifiers
        call l:Notify(l:dest_mode, l:dest_var)
      endfor
    endif
  endif

  if has_key(l:map, 'expr')
    let l:key = has_key(l:map, 'noremap') ? 'noremap' : 'map'
    let l:map[l:key] = eval(l:map.expr)
  endif

  if has_key(l:map, 'map')
    return l:map.map
  elseif has_key(l:map, 'noremap')
    return s:queue_noremap(l:map.noremap)
  else
    return ''
  endif
endfunction

function! mode_variant#merge_modes(source, modes)
  for [l:mode, l:vardict] in items(a:modes)
    let l:smode = get(a:source, l:mode, {'default': {}})
    let a:source[l:mode] = l:smode

    for [l:variant, l:mapdict] in items(l:vardict)
      let l:svar = get(l:smode, l:variant, {})
      let l:smode[l:variant] = l:svar

      for [l:key, l:map] in items(l:mapdict)
        let l:dict = copy(l:map)

        " DSL support: do not produce output for <Nop>s
        if has_key(l:dict, 'noremap') && l:dict.noremap == '<Nop>'
          unlet l:dict.noremap
        endif

        " Parse <> keys so they will be emitted correctly later
        for l:k in ['map', 'noremap']
          if has_key(l:dict, l:k)
            let l:dict[l:k] = eval('"'.escape(l:dict[l:k], '\<"').'"')
          endif
        endfor

        let l:svar[l:key] = l:dict
      endfor
    endfor
  endfor
endfunction

function! s:apply_modes(buffer)
  let l:op = 'map <expr>' . (a:buffer ? ' <buffer>' : '')
  for [l:mode, l:vars] in items(a:buffer ? b:mode_variants : s:modes)
    let l:mapped = []
    for [l:variant, l:mapdict] in items(l:vars)
      for [l:str, l:map] in items(l:mapdict)
        if index(l:mapped, l:str) == -1
          let l:mapped += [l:str]

          " Replace <> keys so that their original text will be passed to handle_input
          let l:arg = substitute(substitute(l:str, '<', '<lt>', 'g'), "'", "''", 'g')
          execute l:mode.l:op l:str "\<SID>handle_input('".l:mode."','".l:arg."')"
        endif
      endfor
    endfor
  endfor
endfunction

function! mode_variant#add_modes(modes)
  call mode_variant#merge_modes(s:modes, a:modes)
endfunction

function! mode_variant#apply_modes()
  call s:apply_modes(v:false)
endfunction

function! mode_variant#add_buffer_modes(modes)
  if !exists('b:mode_variants')
    call s:init_buf()
  endif

  let l:source = copy(b:mode_variants)
  let b:mode_variants = l:source
  call mode_variant#merge_modes(l:source, a:modes)
endfunction

function! mode_variant#apply_buffer_modes()
  if !exists('b:mode_variants')
    call s:init_buf()
  endif

  if (b:mode_variants isnot s:modes)
    call s:apply_modes(v:true)
  endif
endfunction

function! mode_variant#on_variant_change(fn)
  let s:variant_notifiers += [a:fn]
endfunction
let s:fuzzy_modes = {
      \ 'o': '^no',
      \ 'n': '^[n!]',
      \ 'x': "^[Vv\<C-v>]",
      \ 's': "^[Ss\<C-s>]",
      \ 'v': "^[Vv\<C-v>Ss\<C-s>]",
      \ 'i': '^[iR]',
      \ 'c': '^[cr]',
      \ 'l': '^[iRcr]',
      \ 't': '^t'
      \ }
function! mode_variant#fuzzy_variant()
  let l:mode = mode()

  for [l:mmode, l:pat] in items(s:fuzzy_modes)
    if l:mode =~# l:pat
      let l:var = get(s:active_variant, l:mmode, 'default')
      if l:var != 'default'
        return l:var
      endif
    endif
  endfor

  return 'default'
endfunction
