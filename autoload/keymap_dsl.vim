" keymap_dsl.vim: a DSL for specifying hierarchical keymaps
" vim:fdm=marker:ts=2:sts=2:sw=2:et:tw=100

if exists('g:keymap_dsl_loaded')
  finish
endif
let g:keymap_dsl_loaded = v:true

" Convert a string that can be passed to :map to a string of raw bytes.
function! keymap_dsl#eval_key(key)
  return eval('"'.escape(a:key, '\<"').'"')
endfunction

" Mapping stack: properties and modifications are accumulated in a stack.  Stack slots are linked
" when created, and the links are severed after they are popped.
"let s:stack = {'mergenext': v:false}
function! s:frame(mergenext)
  if s:stack.mergenext
    let l:frame = s:stack
  else
    let l:frame = copy(s:stack)
    let l:frame.parent = s:stack
  endif
  let l:frame.mergenext = a:mergenext
  let s:stack = l:frame
  return l:frame
endfunction
function! s:push(key, value, mergenext)
  let l:stack = s:frame(a:mergenext)
  let l:stack[a:key] = a:value
endfunction
function! s:extend(dict, mergenext)
  let l:stack = s:frame(a:mergenext)
  call extend(l:stack, a:dict)
endfunction
function! s:pop()
  if !has_key(s:stack, 'parent')
    echoerr 'Attempt to pop root frame!'
    return
  endif
  let l:stack = s:stack.parent
  unlet s:stack.parent
  unlet s:stack.mergenext
  let s:stack = l:stack
endfunction
" Pretend we pushed something then popped it
function! s:skip_pushpop()
  if s:stack.mergenext
    call s:pop()
  endif
endfunction

function! s:error_stack(msg)
  let l:msg = a:msg
  for l:key in ['modes', 'keys', 'flags', 'type', 'when']
    if has_key(s:stack, l:key)
      let l:msg .= '; '.l:key.': '.string(s:stack[l:key])
    endif
  endfor
  echoerr l:msg
endfunction

function! s:push_key(bang, ...)
  let l:key = a:000
  let l:old = get(s:stack, 'keys', [])

  if len(l:old) == 0
    " nothing
  elseif len(l:old) == 1
    " Single old, multiple new: use old as prefix
    let l:key = map(copy(l:key), {_,v->l:old[0].v})
  elseif len(l:key) == 1
    " Multiple old, single new: use new as suffix
    let l:key = map(copy(l:old), {_,v->v.l:key[0]})
  elseif len(l:old) == len(l:key)
    " Multiple old, multiple new: concatenate pairs
    let l:key = call map(copy(l:key), {i,v->l:old[i].v})
  else
    " We _could_ use the cartesian product, but why?
    call s:error_stack('Cannot apply '.string(l:key).' to existing keys')
    let l:key = l:old
  endif

  call s:push('keys', l:key, a:bang)
endfunction

function! s:push_mod(bang, ...)
  let l:prefix =
        \ (index(a:000, 'meta') != -1 ? 'M-' : '') .
        \ (index(a:000, 'ctrl') != -1 ? 'C-' : '') .
        \ (index(a:000, 'shift') != -1 ? 'S-' : '')

  let l:old = get(s:stack, 'keys', [])
  if l:old == []
    call s:error_stack('No key set for modifiers '.string(a:000))
    let l:key = l:old
  else
    let l:key = map(copy(l:old), {_,v->
          \ (v !~# '<[^<>]*>$')
          \ ? substitute(v, '.$', '<'.l:prefix.'&>', '')
          \ : substitute(v, '<\([^<>]*\)>$', '<'.l:prefix.'\1>', '')
          \ })
  endif

  call s:push('keys', l:key, a:bang)
endfunction

function! s:push_mode(bang, ...)
  if has_key(s:stack, 'modes')
    call s:error_stack('Cannot add additional modes '.string(a:000))
    let l:modes = s:stack.modes
  else
    let l:modes = a:000
  endif
  call s:push('modes', l:modes, a:bang)
endfunction

function! s:push_cond(bang, ...)
  if !a:0
    call s:error_stack('Condition requires at least one argument')
    return
  endif

  let [l:cond; l:args] = a:000
  let l:invert = l:cond[0] == '!'
  let l:cond = l:invert ? l:cond[1:-1] : l:cond

  if !has_key(s:conditions, l:cond)
    call s:error_stack('Invalid Condition '.l:cond)
    return
  endif

  let l:Func = s:conditions[l:cond]
  let l:val = call(l:Func, l:args)

  if type(l:val) == v:t_number || type(l:val) == v:t_bool
    " If the condition failed, emit a fixed zero.  s:push_map will skip these mappings.
    if !l:val == !l:invert
      call s:push('when', 0, a:bang)
    else
      call s:frame(a:bang)
    endif
  elseif type(l:val) == v:t_string
    " Add another term to the condition without overwriting fixed zeros.
    let l:val = (l:invert ? '!' : '') . '('.l:val.')'
    if has_key(s:stack, 'when')
      if s:stack.when isnot 0
        call s:push('when', s:stack.when . ' && ' . l:val, a:bang)
      endif
    else
      call s:push('when', l:val, a:bang)
    endif
  else
    call s:error_stack('Condition '.l:cond.' resulted in wrong type '.type(l:val))
    return
  endif
endfunction

function! s:do_pop(bang)
  call s:pop()
  if a:bang
    while has_key(s:stack, 'parent')
      call s:pop()
    endwhile
  endif
endfunction

let s:map_args = [ '<buffer>', '<nowait>', '<silent>', '<script>', '<expr>', '<unique>', '<Cmd>' ]
function! s:push_map(type, ...)
  if !has_key(s:apply_fns, a:type)
    echoerr 'Unknown mapping type '.a:type
    call s:extend({}, v:false)
    call s:pop()
    return
  endif
  let l:apply = s:apply_fns[a:type]

  let l:flags = a:000
  let l:maps = []
  for l:idx in range(a:0)
    if (index(s:map_args, a:000[l:idx]) == -1)
      let l:flags = l:idx ? a:000[0:l:idx-1] : []
      let l:maps = a:000[l:idx:-1]
      break
    endif
  endfor

  if l:apply.needs_map
    if len(l:maps) == 0
      call s:error_stack('Nothing to map')
      return
    elseif len(l:maps) != len(get(s:stack, 'keys', []))
      call s:error_stack('Wrong number of keys for map '.string(a:000))
      return
    endif
  endif

  if l:apply.needs_mode && len(get(s:stack, 'modes', [])) == 0
    call s:error_stack('No modes selected for map '.string(a:000))
    return
  endif

  " If the conditions are already known to have failed, don't bother creating the map.
  if get(s:stack, 'when', '') isnot 0
    let l:ext = {}
    let l:ext.type = a:type
    let l:ext.maps = l:apply.needs_map ? l:maps : repeat([''], len(s:stack.keys))
    if len(l:flags)
      let l:ext.flags = l:flags
    endif

    call s:extend(l:ext, v:false)
    call extend((index(l:flags, '<buffer>') != -1) ? s:bufmaps : s:maps, [s:stack])
    call s:pop()
  else
    call s:skip_pushpop()
  endif
endfunction

function! s:notify_apply_fns(event, ...)
  for [l:key, l:fn] in items(s:apply_fns)
    if has_key(l:fn, a:event)
      call call(l:fn[a:event], a:000)
    endif
  endfor
endfunction
function! s:concat_apply_fns(event, ...)
  let l:lines = []
  for [l:key, l:fn] in items(s:apply_fns)
    if has_key(l:fn, a:event)
      let l:lines += call(l:fn[a:event], a:000)
    endif
  endfor
  return filter(l:lines, {_,v->v != ''})
endfunction

function! s:apply_each_map(map)
  let l:lines = []
  let l:flags = join(get(a:map, 'flags', []))
  let l:apply = s:apply_fns[a:map.type]
  for l:mode in get(a:map, 'modes', [''])
    let l:sd = {
          \ 'stack': a:map,
          \ 'mode': l:mode,
          \ 'flags': join(get(a:map, 'flags', []))
          \ }
    for l:idx in range(len(a:map.keys))
      let l:sd.key = a:map.keys[l:idx]
      let l:sd.map = a:map.maps[l:idx]
      let l:lines += [l:apply.apply(l:sd)]
    endfor
  endfor

  return filter(l:lines, {_,v->v != ''})
endfunction

function! s:apply_maps(list, buffer)
  call s:notify_apply_fns('begin', a:buffer)

  for l:dict in a:list
    if !eval(get(l:dict, 'when', 1))
      continue
    endif

    call execute(s:apply_each_map(l:dict))
  endfor

  call s:notify_apply_fns('apply_finish', a:buffer)
endfunction

function! s:sort_condition(a, b)
  let l:a = get(a:a, 'when', '')
  let l:b = get(a:b, 'when', '')
  return (l:a == l:b) ? 0 : (l:a > l:b) ? 1 : -1
endfunction
function! s:cache_maps(list, indent, buffer)
  " Group counditions so we can merge if statements together
  " XXX this isn't stable enough for e.g. Unmap/Map to keep their order.  If this is an actual
  " problem, a 'sorts_first' key to push Unmap to the beginning might help?
  let l:sorted = sort(copy(a:list), function('s:sort_condition'))

  let l:lines = []
  let l:pfx = repeat(' ', a:indent)

  let l:i = 0
  let l:len = len(l:sorted)
  while l:i < l:len
    " Filter out constant conditions and emit an if statement if needed
    let l:cond = get(l:sorted[l:i], 'when', 1)
    if l:cond is 0
      let l:i += 1
      continue
    elseif l:cond isnot 1
      let l:lines += [l:pfx.'if ' . l:cond]
      let l:pfx = repeat(' ', a:indent + 2)
    else
      let l:pfx = repeat(' ', a:indent)
    endif

    call s:notify_apply_fns('begin', a:buffer)

    let l:maplines = []
    while l:i < l:len && get(l:sorted[l:i], 'when', 1) is l:cond
      let l:maplines += map(s:apply_each_map(l:sorted[l:i]), {_,v->l:pfx.v})
      let l:i += 1
    endwhile

    let l:pfx = repeat(' ', a:indent)
    let l:prelines = map(s:concat_apply_fns('cache', a:buffer), {_,v->l:pfx.v})
    let l:lines += l:prelines + l:maplines

    if l:cond isnot 1
      let l:lines += [l:pfx.'endif']
    endif
  endwhile

  let l:lines += s:concat_apply_fns('cache_finish', a:buffer)

  return l:lines
endfunction

function! s:dsl_apply(type, dict)
  return a:dict.mode.a:type.' '.a:dict.flags.' '.a:dict.key.' '.a:dict.map
endfunction

function! keymap_dsl#load()
  let s:maps = []
  let s:bufmaps = []
  let s:stack = {'mergenext': v:false}
  let s:apply_fns = {}
  let s:conditions = {}
  call keymap_dsl#add_map_fn('map', {v->s:dsl_apply('map', v)})
  call keymap_dsl#add_map_fn('noremap', {v->s:dsl_apply('noremap', v)})
  call keymap_dsl#add_map_fn('unmap', {v->v.mode.'unmap '.v.flags.' '.v.key},
        \ {'needs_map': v:false})

  command! -nargs=+ -bang -bar Key call s:push_key(<bang>0, <f-args>)
  command! -nargs=+ -bang -bar Modifier call s:push_mod(<bang>0, <f-args>)
  " TODO: WithModifier to add modified variants to key list?
  command! -nargs=+ -bang -bar Mode call s:push_mode(<bang>0, <f-args>)
  command! -nargs=+ -bang -bar Condition call s:push_cond(<bang>0, <f-args>)
  command! -nargs=0 -bang -bar Pop call s:do_pop(<bang>0)
  command! -nargs=+ Map call s:push_map('map', <f-args>)
  command! -nargs=+ Noremap call s:push_map('noremap', <f-args>)
  command! -nargs=+ MapFor call s:push_map(<f-args>)
  command! -nargs=* Unmap call s:push_map('unmap')
endfunction

function! keymap_dsl#unload()
  delcommand Key
  delcommand Modifier
  delcommand Mode
  delcommand Condition
  delcommand Pop
  delcommand Map
  delcommand Noremap
  delcommand MapFor

  " Leave bufmaps so it can be reapplied later
  unlet! s:maps s:stack s:apply_fns s:conditions
endfunction

function! keymap_dsl#push(dict, mergenext)
  call s:extend(a:dict, a:mergenext)
endfunction

function! keymap_dsl#add_map_fn(type, apply, ...)
  let l:extra = {}
  if a:0 == 1 && type(a:1) == v:t_dict
    let l:extra = a:1
  elseif a:0
    echoerr 'Extra parameter must be a dictionary'
    return
  endif

  if has_key(s:apply_fns, a:type)
    echoerr a:type.' already defined'
    return
  endif

  let s:apply_fns[a:type] = extend({
        \ 'needs_mode': v:true,
        \ 'needs_map': v:true,
        \ 'apply': a:apply,
        \}, l:extra)
endfunction

function! keymap_dsl#add_config_fn(type, apply)
  call keymap_dsl#add_map_fn(a:type, a:apply, { 'needs_mode': v:false })
endfunction

function! keymap_dsl#map(...)
  return call('s:push_map', a:000)
endfunction

function! keymap_dsl#add_condition(name, func)
  let s:conditions[a:name] = a:func
endfunction

function! keymap_dsl#apply()
  call s:apply_maps(s:maps, v:false)
  let s:bufcmds = s:cache_maps(s:bufmaps, 0, v:true)
endfunction

function! keymap_dsl#apply_buffer()
  call execute(s:bufcmds, '')
endfunction

function! keymap_dsl#cache_global()
  return s:cache_maps(s:maps, 0, v:false)
endfunction

" Buffer maps will be applied repeatedly, so return a function for convenience.
function! keymap_dsl#cache_buffer(name)
  return ['function! '.a:name.'()']
        \ + s:cache_maps(s:bufmaps, 2, v:true)
        \ + ['endfunction']
endfunction
