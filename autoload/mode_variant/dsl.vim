" mode_variant/dsl.vim: glue between mode_variant and keymap_dsl
" vim:fdm=marker:ts=2:sts=2:sw=2:et:tw=100

function! s:dsl_begin(buffer)
  let s:dsl_dict = {}
endfunction
function! s:dsl_apply(type, map)
  let l:pd = a:map.stack

  let l:map = {}
  if index(get(l:pd, 'flags', []), '<expr>') == -1
    let l:map[a:type] = a:map.map
  else
    let l:map[a:type] = ''
    let l:map.expr = a:map.map
  endif

  if has_key(l:pd, 'mv_sets_mode')
    let l:map.mode = l:pd.mv_sets_mode
  endif
  if has_key(l:pd, 'mv_sets_variant')
    let l:map.variant = l:pd.mv_sets_variant
  endif
  let l:dict = { a:map.mode: { get(l:pd, 'mv_variant', 'default'): { a:map.key: l:map } } }

  call mode_variant#merge_modes(s:dsl_dict, l:dict)
  return ''
endfunction
function! s:dsl_apply_finish(buffer)
  if !empty(s:dsl_dict)
    let l:fn = a:buffer ? 'buffer_modes' : 'modes'
    call call('mode_variant#add_'.l:fn, [s:dsl_dict])
    call call('mode_variant#apply_'.l:fn, [])
  endif
  unlet s:dsl_dict
endfunction
function! s:dsl_cache(buffer)
  if empty(s:dsl_dict)
    return ['']
  else
    let l:fn = a:buffer ? 'add_buffer_modes' : 'add_modes'
    return ['call mode_variant#'.l:fn.'('.string(s:dsl_dict).')']
  endif
  unlet s:dsl_dict
endfunction
function! s:dsl_cache_finish(buffer)
  let l:fn = a:buffer ? 'apply_buffer_modes' : 'apply_modes'
  return ['call mode_variant#'.l:fn.'()']
endfunction

function! mode_variant#dsl#load()
  call keymap_dsl#add_map_fn('mv_map', {v->s:dsl_apply('map', v)}, {
        \ 'begin': function('s:dsl_begin'),
        \ 'apply_finish': function('s:dsl_apply_finish'),
        \ 'cache': function('s:dsl_cache'),
        \ 'cache_finish': function('s:dsl_cache_finish')
        \})
  call keymap_dsl#add_map_fn('mv_noremap', {v->s:dsl_apply('noremap', v)})

  " FIXME: multiple variants
  command! -nargs=1 -bang -bar MVVariant
        \ call keymap_dsl#push({'mv_variant': <f-args>}, <bang>0)
  command! -nargs=1 -bang -bar MVSetMode
        \ call keymap_dsl#push({'mv_sets_mode': <f-args>}, <bang>0)
  command! -nargs=1 -bang -bar MVSetVariant
        \ call keymap_dsl#push({'mv_sets_variant': <f-args>}, <bang>0)
  command! -nargs=+ MVMap
        \ call keymap_dsl#map('mv_map', <f-args>)
  command! -nargs=+ MVNoremap
        \ call keymap_dsl#map('mv_noremap', <f-args>)
endfunction
function! mode_variant#dsl#unload()
  delcommand MVVariant
  delcommand MVSetMode
  delcommand MVSetVariant
  delcommand MVMap
  delcommand MVNoremap

  unlet! s:dsl_dict
endfunction
